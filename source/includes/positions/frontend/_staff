:markdown

  ## Staff
  A Senior Frontend Engineer will be promoted to a Staff Frontend Engineer when he/she has
  demonstrated significant leadership to deliver high-impact projects. This may
  involve any type of consistent "above and beyond senior level" performance,
  for example:
  1. Technical Skills
      * Identifies significant projects that result in substantial cost savings or revenue
      * Able to create innovative solutions that push GitLab's technical abilities ahead of the curve
  2. Leadership
      * Leads the design for medium to large projects with feedback from other engineers
      * Working across functional groups to deliver the project
  3. Code quality
      * Proactively identifying and reducing technical debt
      * Proactively defining and solving important architectural issues
  4. Communication
      * Writing in-depth documentation that shares knowledge and radiates GitLab technical strengths
  5. Performance & Scalability
      * Leads development of projects that lead to a significant improvement of the overall
        performance and scalability of GitLab
