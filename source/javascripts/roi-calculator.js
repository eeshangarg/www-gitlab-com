(function() {
  var paramsFetcher = function() {
    var regex = /[?&]([^=#]+)=([^&#]*)/g;
    var params = {};
    var match = '';

    while(match = regex.exec(window.location.href)) {
      params[match[1]] = match[2];
    }

    return (Object.keys(params).length > 0) ? params : null;
  };

  var paramsParser = function(params) {
    if (!params) return null;
    var validParams = {
      teamInfo: {},
      features: {},
    };

    Object.keys(params).forEach(function(param) {
      if (param === 'team_size' || param === 'developer_cost') {
        validParams.teamInfo[param] = params[param];
      } else if (params[param] ===  '1') {
        validParams.features[param] = params[param];
      }
    });

    return validParams;
  }

  this.RoiCalculator  = (function() {
    function RoiCalculator(params) {
      this.$featuresTable = $('.features-table table tr');
      this.$featuresTableInputs = $('.include input', this.$featuresTable);
      this.$teamSizeInput = $('.js-developer-team-size');
      this.$costDeveloperInput = $('.js-cost-per-developer');
      this.$savingsText = $('.savings-text h3');
      this.$copyLinkButton = document.querySelector('.js-copy-link');
      this.params = params;
      this.$featuresTableInputs.on('click', this.calculateSavings.bind(this));

      if(this.params) {
        this.fillValues.call(this);
        this.calculateSavings.call(this);
      }
    }

    RoiCalculator.prototype.calculateSavings = function() {
      var totalSavings = 0;
      var costPerDeveloper = parseFloat(this.$costDeveloperInput.val() || 0);
      this.$featuresTable.each(function() {
        var currentFeatureCheckbox = $('.include input', this);
        var currentFeatureSavings = $('.savings', this);
        if (currentFeatureCheckbox.is(':checked')) {
          var incidents = parseFloat(currentFeatureCheckbox.data('incidents-year'));
          var hours = parseFloat(currentFeatureCheckbox.data('hours-incident'));
          var result = incidents * hours * costPerDeveloper;
          currentFeatureSavings.text('$' + result.toFixed(2));
          totalSavings = totalSavings += result;
        }
      });
      totalSavings = totalSavings * parseFloat(this.$teamSizeInput.val() || 0);
      this.$savingsText.text(this.formatAmount(totalSavings) + ' potential savings per year');
      this.createUrl.call(this);
    }

    RoiCalculator.prototype.createUrl = function() {
      var link = this.rootUrl() + '?';
      this.$featuresTable.each(function() { 
        var currentFeatureCheckbox = $('.include input', this);
        if(currentFeatureCheckbox.is(':checked')) {
          link = link += currentFeatureCheckbox.data('link-shorthand') + '=1&';
        }
      });
      link = link += 'team_size=' + (this.$teamSizeInput.val() || 0) + '&';
      link = link += 'developer_cost=' + (this.$costDeveloperInput.val() || 0);
      this.$copyLinkButton.dataset.clipboardText = link;
      new Clipboard('.js-copy-link');
    }

    RoiCalculator.prototype.fillValues = function() {
      Object.keys(this.params.features).forEach(function(param) {
        this.$featuresTableInputs.filter('[data-link-shorthand="'+ param +'"]').prop('checked', true);
      }.bind(this));
      this.$costDeveloperInput.val(this.params.teamInfo.developer_cost);
      this.$teamSizeInput.val(this.params.teamInfo.team_size);
    }

    RoiCalculator.prototype.rootUrl = function() {
      var url = window.location.href;
      if (url.indexOf('?') >= 0) {
        url = url.split("?")[0];
      }
      return url;
    }

    RoiCalculator.prototype.formatAmount = function(amount) {
      return '$' + amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    return RoiCalculator;
  })();

  // Add the params once we get the basic functionality going
  var urlParams = paramsFetcher();
  var parsedParams = paramsParser(urlParams);
  
  if(parsedParams) {
    new RoiCalculator(parsedParams);
  } else {
    new RoiCalculator(null);
  }
  
})();
