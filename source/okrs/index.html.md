---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## What are OKRs?

OKRs are our quarterly goals to execute our [strategy](https://about.gitlab.com/strategy/). The make sure our goals are clearly defined and aligned throughout the organization. For more information see [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal). The OKRs are our quarterly goals

## Format

`  Owner: Key Result as a sentence. Metric`

- We use two spaces to indent instead of tabs.
- OKRs start with the owner of the key result. When referring to a team lead we don't write 'lead' since it is shorter and the team goal is the same.
- The key result can link to an issue.
- The metric can link to real time data about the current state.
- The three company/CEO objectives are level 3 headers to provide some visual separation.

## Levels

We only list your key results, these have your (team) name on them.
Your objectives are the key results under which your key results are nested, these should normally be owned by your manager.
We do OKRs up to the team or director level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Part of the individual performance review is the answer to: how much did this person contribute to the team key objects?
We have no more than [five layers in our team structure](https://about.gitlab.com/team/structure/).
Because we go no further than the team level we end up with a maximum 4 layers of indentation on this page (this layer count excludes the three company/CEO objectives).
The match of one "nested" key result with the "parent" key result doesn't have to be perfect.
Every team should have at most 9 key results. To make counting easier only mention the team and people when they are the owner.
The advantage of this format is that the OKRs of the whole company will fit on 3 pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them.
To update: make a merge request and assign it to the CEO.
If you're a [team member](https://about.gitlab.com/team/) or in the [core team](https://about.gitlab.com/core-team/) please post a link to the MR in the #ceo channel.

At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

Timeline of how we draft the OKRs:

1. Executive team pushes updates to this page: 4 weeks before the start of the quarter
1. Executive team 90 minute planning meeting: 3 weeks before the start of the quarter
1. Discuss with the board and the teams: 2 weeks before the start of the quarter
1. Executive team 'how to achieve' presentations: 1 week before the start of the quarter
1. Add Key Results to top of 1:1 agenda's: before the start of the quarter
1. Present OKRs at a functional group update: first week of the quarter
1. Present 'how to achieve' at a functional group update: during first three weeks of the quarter
1. Review previous quarter and next during board meeting: after the start of the quarter

## Critical acclaim

Spontaneous chat messages from team members after introducing this:

- As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up
- I like it too, especially the fact that it is in one page, and that it stops at the team level.
- I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.
- I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."
- I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

## 2017-Q3 DRAFT

Summary: With a great team make a popular next generation product that grows revenue.

TODO: Link to Diff with the start of the quarter

### Objective 1: Grow incremental ACV according to plan.

* CEO: Build SQL pipeline for new business. 300% of IACV plan
  * CMO: Identify CE installations within strategic organizations with installations with over 500 users and launch demand generation campaigns.
  * CMO: More demand coming from inbound marketing activities. 50% of sales pipeline quota
    * CMO: Increase EE Trials. 21% QoQ
    * CMO: Increase EE Trial SQL $ pipeline contribution 43% QoQ
  * CFO: Forecast accuracy and delivery of relevant information
    * Controller: Zuora to Salesforce reconciliation complete and tested.
    * CFO: Forecasting model projects forecast +/- 10% normalized for big deals.
    * Controller: Zuora to Salesforce to Zendesk data synch completed.
  * CFO: Deliver 10 SQLs > $10k during quarter through investor outreach
  * VP Prod: Salesforce shows usage. [Version check](https://gitlab.com/gitlab-com/salesforce/issues/104), usage ping, and ConvDev index are available in SFDC.
  * CRO: 100% of quota-carrying reps at 3x pipeline of quarterly iACV goal
    * Alliances: generates 15% of sales pipeline
    * Federal: generates 5% of sales pipleine
    * Customer Success: generate 15% of sales pipeline quota
    * SDR lead: Generate 15% of sales pipeline quota
      * SDR lead: Get an SQL from Fortune 1000 companies identified using CE. 50% of companies.
      * SDR lead: Increase win rate. 20%
  * CRO: Increase new business sales velocity. Grow 200% YoY to 602 new business deals closed won.
  * CRO: Decrease age of non-web direct deals by 10%
    * Sales Ops: New sales stages and exit criteria. Launch and Train
    * Customer Success: Solution Architects managing PoC's for new business opps within strategic/large accounts. 50%
    * PMM: 2 ROI calculators
    * PMM: 3 case studies
  * CRO: Keep sales efficient. Sales efficiency ratio > 1.8 (IACV / sales spend)
  * CMO: Keep marketing efficient. Marketing efficiency ratio > 3.4 (IACV / marketing spend)
* CEO: Understand and improve existing account growth. 200% YoY
  * VP Eng: Geo DR successful deployments. [Test fail-over with GitLab.com](https://gitlab.com/gitlab-org/gitlab-ee/issues/1884)
    * Platform: Get Geo reliable with a testing framework.
    * Build: Deliver PostgreSQL HA in omnibus and Terraform to enable HA
  * VP Prod: [Make it easier to discover and use EE features](https://gitlab.com/gitlab-org/gitlab-ee/issues/2417). Ship EE by default. Trials increase 100%.
    * Technical Writing: CE docs link to EE wherever relevant. Increased visits to EE docs.
  * VP Prod: Ship paid subscriptions for GitLab.com. All EE features are behind subscription. Ability to buy higher storage limits.
  * CFO: Make our sales process data driven
    * CFO: Lead hired
    * Data and Analytics Architect: Data and Analytics vision and plan signed off by executive team
    * Data and Analytics Architect: Create a user journey funnel
  * Head of Prod: [Make sure the installation process is great](https://docs.google.com/document/d/1f8cGiPV357krBsvqxViEsxBZukZGMNgykzy9rJoEpWg/edit). Do all TODOs
  * CMO: Educational Email campaign series to educate users on full solution capabilities and how to get started. TODO # of campaign conversions
  * Customer Success: Create report in Salesforce. Identify accounts less than 70% license utilization and create plays and tasks to increase usage.
  * Customer Success: Create report in Salesforce. Identify, engage and ensure proper sales stage for every renewal valued at over $20,000 in ACV
  * Customer Success: Create report in Salesforce. Identify, engage and create growth plans for every renewal within the large/strategic segmentation where we have less than 50% license penetration.
  * Customer Success: Identity the trigger(s) to purchase for large/strategic accounts
  * Support: Provide faster, more knowledgable support. Reduce Average Time to Solve by 5%.
* CEO: Increase ASP (IACV per won) by 25% QoQ
    * CRO: Triple average new business deal size YoY to $4,875
    * CRO: 50% of iACV from EEP
    * CMO: More sales assets. 3 new for each stage (TOFU, MOFU, BOFU) of the sales process
      * CMO: [Presentation generation based on conversational development index.](https://gitlab.com/gitlab-com/organization/issues/95)
      * CMO: 3 minute video product demo targeted at buyer audience. Published
    * Sales Demo easy to give. 75% of company passes test.
    * VP Prod: Conversational Development Index in product and SFDC.
    * VP Prod: Improve JIRA support. Better than Bitbucket. Support transitions, references and development panel.
      * Technical writing: Create a page on our JIRA integration and how to move to Issues. 100% increased usage of JIRA integration.

### Objective 2: Popular next generation product.

* CEO: GitLab.com ready for mission-critical tasks. 99% of user requests < 1s
  * VP Scaling: Highly Available. [99.9%](http://stats.pingdom.com/81vpf8jyr1h9/1902794/history)
    * Production: HA setup for Redis
    * Production: Robust backups with automatic periodic restores. Counter on monitor.gitlab.net shows days since last automatic restore.
    * Production: Multi-canary deployments enabled. PoC of two canaries via kubernetes with Build team's Helm charts.
    * Production: Enable GitLab.com search using ElasticSearch. Provide a cluster that is 99% available for > 2 months in Q3.
    * Database: Run pgbouncer from Omnibus. Shipped
    * Database: Use Omnibus provided software for HA / failover instead of using a GitLab.com specific setup. Shipped and In Use
  * VP Eng: Enable High Availability
    * Platform: Enable graceful degradation when file servers are down
    * Platform: Support multiple Redis clusters for persistent and cache stores
    * Platform: Use Geo DR to move between clouds and AZs
    * Support: Improve GitLab provided debugging tools. We'll log all times we need terminal access and create an issue for each to improve GitLab tools.
    * Prometheus: Allow monitoring of Unicorn, Sidekiq, and gitlab-shell. Deprecate InfluxDB
    * Edge: Make GitLab QA test backup/restore, LDAP, Container Registry, and Mattermost
    * CI/CD: Make [runners work on Google Compute Engine without dying halfway](https://gitlab.com/gitlab-com/infrastructure/issues/1936)
    * CI/CD: Decrease monthly per minute cost of shared runners by 30%
    * CI/CD: 99% of pipelines without any existing builds start within 1 s
    * CI/CD: Track and ensure that number of job failures due to system failure is less than 0.01%
  * VP Scaling: [Lower latency](https://gitlab.com/gitlab-com/infrastructure/issues/947). [99% of user requests < 1 second](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=2&fullscreen&orgId=1)
    * Gitaly: Gitaly service active on file-servers
    * Gitaly: Roll out Gitaly migrations. 24 additional endpoints migrated to Gitaly and [in acceptance testing](https://gitlab.com/gitlab-org/gitaly/blob/master/README.md#current-features)
    * Gitaly: Reduce “idea to production” time of migrations. 80% of all migrations started in Q3 are enabled on GitLab.com within two GitLab releases.
    * Database: Reduce the p99 of [SQL timings](https://performance.gitlab.net/dashboard/db/daily-overview?panelId=12&fullscreen&orgId=1) across the board to 200 ms (100-200 ms less than what we have now).
  * VP Eng: Lower latency in application
    * Discussion: Solve performance issues. Reduce p95 of [discussion-related actions](https://performance.gitlab.net/dashboard/db/daily-overview?orgId=1) with over 10 hits/day to < 1 s. Reduce p99 to < 3 s.
    * Platform: Solve performance issues. Reduce p95 of [platform-related actions](https://performance.gitlab.net/dashboard/db/daily-overview?orgId=1) with over 10 hits/day to < 1 s. Reduce p99 to < 3 s.
    * Frontend: Measure end-to-end performance of frontend rendering
    * Edge: Ship large database seeder for developers
    * Edge: Enable Bullet by default
  * VP Eng: Eliminate critical stability issues
    * Discussion: Merge requests get merged 100% without ever getting into stuck locked state
    * Platform: Project authorizations fast (< 1 s) and consistent (requires no manual refreshes). DONE
    * Platform: Project imports and forks complete 100% without ever getting stuck
    * Platform: Namespace and project renames work 100% of the time
    * Platform: Repository cache state eventually consistent within minutes (no manual expiration needed)
    * Frontend: Implement cross-browser automated testing. Catch at least one browser regression before release date.
    * CI/CD: In case of runners infrastructure failure, do automatic failover
  * VP Scaling: Secure platform
    * Security: Improve defenses. Implement top 10 actions from Risk Assessment.
    * Security: Vulnerability testing. Conduct external testing.
    * Security: Improve security practices through people and processes. Backlog of security issues reduced by 50% (currently 148 issues in [gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security), 9 in [gitlab-ee](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security), 69 on [infrastructure](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security)).
* CEO: Increased usage of idea to production. 100% growth
  * CMO: Generate more company and product awareness including [overtake BitBucket in Google Trends](https://trends.google.com/trends/explore?q=bitbucket,gitlab).
  * VP Prod: Issue boards usage increase. 100% (.com and usage ping)
  * VP Prod: Service Desk usage increase. 100% (.com and usage ping)
  * VP Prod: Subgroups usage increase. 100% (.com and usage ping)
  * Head Prod: Monitoring usage increase. 100% (.com and usage ping)
    * Prometheus: [Have many metrics enabled by default](https://gitlab.com/gitlab-org/gitlab-ce/issues/30369).
    * Prometheus: [Single click deploy of Prometheus](https://gitlab.com/gitlab-org/gitlab-ce/issues/28916)
  * Head Prod: Pipelines usage increase. 100% (.com and usage ping)
    * CI/CD: [Improve onboarding](https://gitlab.com/gitlab-org/gitlab-ce/issues/32638). 10 issues shipped.
    * CI/CD: [One-click Spring, Node, Ruby templates](https://gitlab.com/gitlab-org/gitlab-ce/issues/32420). Shipped.
  * Head Prod: Environments usage increase. 100% (.com and usage ping)
  * Head Prod: Review apps usage increase. 100% (.com and usage ping)
  * CTO: Automatic code quality monitoring. Shipped.
  * CTO: PGP signing support. Shipped.
  * CTO: Improve code quality and test coverage. 20 merge requests.
  * CMO: Better explain our solution, features and value proposition. Flow page that links to feature pages.
  * UX: [Measure usability of critical user flows to identify areas needing improvement.](https://gitlab.com/gitlab-org/ux-research/issues/13)
        * Propose optimizations of critical user flows based on results found in research.
        * Implement optimization and re-measure to ensure the user experience has improved.
  * Build: Simplify HTTPS configuration. In Omnibus and Helm. For Rails app, container registry, and pages. [Consider Let's Encrypt](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1096).
  * Dir Partnerships: First team of OSS projects starts using GitLab for proof-of-concept/testing at Drupal, Gnome and Kubernetes.
  * Dir Partnerships: Get major partner to use it for CI.
  * Dir Partnerships: [AWS QuickStart guide](https://gitlab.com/gitlab-org/gitlab-ce/issues/29199) published
  * CMO: More contributors from the wider community each month. Unique contributors grow 10% QoQ
  * CMO: Initiate AR engagement with key analysts and achieve Leader in the Forrester Wave CI research report.
  * Head Prod: [Make sure we have a very quick install of all our components](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2432)
  * VP Eng: Make sure existing features are used by GitLab.com. Cycle time analytics working.
    * Production: GitLab.com uses Kubernetes. In use.
    * Production: GitLab.com uses Canary deploys. In use.
    * Production: GitLab.com uses Review apps. In use.
    * Support: GitLab.com uses Service Desk. In use for one process.
    * Build: Deliver Helm charts which are used on GitLab.com.
    * CI/CD: Help GitLab.com use our deployment features. CD/Kubernetes/Helm/Canary deploys/Review Apps/Service Desk
    * Edge: GDK based on minikube
    * UX: [Make it easier to find and use advanced GitLab features](https://gitlab.com/gitlab-org/gitlab-ce/issues/25341)
  * Head Prod: Ensure all features are usable by GitLab.com
* CEO: Next generation product. Ship [Auto DevOps demo](https://gitlab.com/gitlab-com/www-gitlab-com/issues/1452).
  * Head Prod: Marketing copy for [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/2517). Published.
  * UX: [Improved navigation. Iterate on it every month.](https://gitlab.com/gitlab-org/gitlab-ce/issues/32794)
  * UX: [Improve perceived performance](https://gitlab.com/gitlab-org/gitlab-ce/issues/29666)
  * CI/CD: [Java artifact repository](https://gitlab.com/gitlab-org/gitlab-ce/issues/19095). Maven compatible first iteration.
  * Head Prod: [Make vision video](https://gitlab.com/gitlab-org/gitlab-ce/issues/32640). Published.
  * Head Prod: [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/2517) plans. 10 issues worked out.

### Objective 3: Great team.

* CEO: Effective leadership. NPS of leaders as rated by their reports 10% improvement
  * Sr.Dir.PO: Management completed basic management (MGR) classes. 75% completed
  * Sr.Dir.PO: 5 GitLab specific courses for the management team.  Published and delivered.
  * CFO: [Real-time analytics platform](https://about.gitlab.com/handbook/finance/analytics/). 40% of metrics live
* CEO: Attract great people. New hire score 2% improvement
  * Sr.Dir.PO: More sourced recruiting. 20% of total hires
  * Sr.Dir.PO: Hires from lower cost locations. 20% increase
  * Sr.Dir.PO: Great recruiting process. NPS of applications 5%
  * Sr.Dir.PO: Structured interview questions. Implemented for 4 positions
  * Sr.Dir.PO: Interviewer rating (ELO). Implemented
  * Sr.Dir.PO: Quicker hiring cycle (plan interviews upfront). Hiring time -25%
  * Sr.Dir.PO: Improve our global compensation framework. Structured on better data.
* CEO: Retain great people. eNPS 0% change
  * CRO: Sales training program to advance sales skills. 80% of sales people trained.
  * Sr.Dir.PO: Improve diversity. Two initiatives deployed.
